# day01_Docker

## 今日任务

- [ ] 了解Docker是什么及Docker的核心组成部分
- [ ] 掌握Docker镜像操作的核心命令
- [ ] 掌握Docker容器操作的核心命令
- [ ] 掌握Docker数据卷和目录映射的命令
- [ ] 掌握如何在Docker上部署SpringBoot项目



## 一、Docker概述

### 01.简介

- Docker是一项开源的容器虚拟化技术
- Docker的优势
  - 一次部署，到处运行
- Docker的核心组成部分
  - 镜像
    - 一种特殊的文件系统
    - 需要从远程仓库下载
    - 相当于Window中安装软件的.exe文件
  - 容器
    - 通过镜像在Docker中安装的软件
    - 如果使用Java的思想类比，镜像就相当于Java类，容器就相当于通过Java类创建的一个一个的实例对象
  - 仓库（注册中心）
    - 保存镜像的地方

### 02.安装

- 安装Docker的步骤

  - 1.安装依赖

    ```shell
    yum install -y yum-utils
    ```

    

  - 2.配置Docker下载源

    ```shell
    yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
    ```

    

  - 3.安装Docker社区版及其他工具

    ```shell
    yum install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
    ```

    

  - 4.启动Docker服务

    ```
    systemctl start docker
    ```

    

  - 5.可以设置Docker服务开机自启

    ```
    systemctl enable docker
    ```

    

## 二、Docker镜像操作核心命令

### 01.配置阿里云镜像加速器

- 在/etc/docker目录下创建一个文件daemon.json

- 在daemon.json文件中添加以下内容

  ```
  {
    "registry-mirrors": ["https://phtv51hj.mirror.aliyuncs.com"]
  }
  ```

  

- 执行以下命令重新加载Docker的守护进程

  ```
  systemctl daemon-reload
  ```

  

- 重启Docker服务

  ```
  systemctl restart docker
  ```



### 02.核心命令

- docker search 镜像的名字:版本
  - 搜索指定版本的镜像
- docker images
  - 查询本地所有的镜像
- docker pull 镜像的名字:版本
  - 下载指定版本的镜像
- docker pull 镜像的名字
  - 下载最新版本的镜像
- docker rmi 镜像的名字:版本
  - 根据镜像的名字删除镜像
- docker rmi 镜像的id
  - 根据镜像的id删除镜像
  - 如果存在通过当前镜像创建的容器，删除镜像时需要添加一个-f的参数进行强制删除
- docker -h 或者 docker --help
  - 查询docker命令的帮助信息



## 三、Docker容器操作核心命令

### 01.核心命令

- docker run -it 镜像的名字:版本
  - 创建一个交互式的容器，名字随机生成
  - 交换的容器创建一会连接上容器如果退出容器自动退出
- docker run -id 镜像的名字:版本
  - 创建一个守护式的容器，名字随机生成
  - 参数说明
    - -it
      - 创建交互式的容器
    - -id
      - 创建守护式的容器
    - --name
      - 指定容器的名字
      - 例如：docker run -id --name=mycentos7 centos:7
    - -p
      - 如果需要通过宿主机连接容器需要通过-p参数指定端口号的映射，因为docker中的容器不能在宿主机直接访问
      - 例如：docker run -id --name=myredis -p 6380:6379 redis:7.0.10
- docker ps
  - 查询正在运行的容器
- docker ps -a
  - 查询所有容器
- docker rm 容器名或容器的id
  - 删除容器
  - 如果要强制删除需要添加 -f 参数
- docker start 容器的名字或容器的id
  - 启动容器
- docker stop 容器的名字或容器的id
  - 关闭容器
- docker exec -it 容器的名字或容器的id  /bin/bash
  - 通过命令行连接容器

### 02.其他命令

- docker logs -f 容器的名字或容器的id
  - 实时监控容器日志的变化
- docker inspect 容器的名字或容器的id
  - 查询容器的详细信息
- docker cp 宿主机中文件的路径  容器名:容器中的路径
  - 将宿主机中的文件拷贝到容器中的某个目录
  - 如果要从容器向宿主机拷贝文件将两个路径的位置颠倒一下即可
- docker commit 容器的名字 新的镜像的名字
  - 将容器保存为新的镜像
- docker save -o 要保存的tar文件的名字和目录  镜像的名字
  - 将某个镜像保存为一个tar文件
- docker load -i tar文件
  - 将tar文件加载为docker镜像

### 03.数据卷和目录挂载相关命令

- 数据卷
  - 数据卷是Docker中的一个虚拟目录，对应着宿主机上一个真实的路径，作用是通过容器中的数据和恢复容器中的数据
- docker volume ls
  - 查询数据卷
- docker volume create 数据卷的名字
  - 创建数据卷
- docker volume inspect 数据卷的名字
  - 查询数据卷的详细信息，比如数据卷的真实路径
- docker volume rm 数据卷的名字
  - 删除数据卷
- docker volume prune
  - 删除空闲的数据卷
- docker run -id --name=容器的名字  -v 数据卷的名字:容器中的路径  镜像名称:版本号
  - 将数据卷挂载到容器的某个目录下
- docker run -id --name=容器的名字  -v /宿主机中的某个路径:容器中的路径  镜像名称:版本号
  - 将宿主机中的某个目录与容器中的某个目录进行挂载
- 数据卷与目录挂载的区别
  - 1.创建数据卷挂载的时候 -v 参数后面的第一个路径不带 /
  - 2.在创建容器的时候，数据卷挂载数据卷和容器中的目录中的内容可以相互同步；目录挂载会将宿主机中的目录同步到容器中的目录



## 四、在Docker上部署SpringBoot项目

### 01.使用dockerfile文件自定义镜像

- dockerfile是什么

  - dockerfile是包含了一系列docker命令的文本文件，通过它可以构建新的镜像

- 使用dockerfile必须指定一个基础镜像，基于它构建新的镜像

- 使用centos:7镜像构建包含jdk17的镜像的步骤

  - 1.将jdk17的安装包上传到宿主机的某个目录下

  - 2.在jdk17安装包所在的目录下创建一个dockerfile文件，文件中的内容如下

    ```shell
    FROM centos:7
    MAINTAINER atguigu
    RUN mkdir -p /usr/local/java
    ADD jdk-17_linux-x64_bin.tar.gz /usr/local/java/
    ENV JAVA_HOME=/usr/local/java/jdk-17.0.8
    ENV PATH=$PATH:$JAVA_HOME/bin
    ```

    

  - 3.执行如下命令

    ```
    docker build -t 新的镜像的名字:版本 .
    ```

    

  - 4.通过新的镜像创建新的容器

    ```
    docker run -it --name=容器的名字 新的镜像的名字:版本 /bin/bash
    ```

    

  - 5.通过javac测试使用安装了jdk17